<?php
/**
 * Mobile - Applications View (Settings)
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 // build settings description list
 $settings_table=new strTable(api_text("applications_view-settings-tr-unvalued"));
 //$settings_table->addHeader("&nbsp;","nowrap",16);
 $settings_table->addHeader(api_text("cMobileApplicationSetting-property-code"),"nowrap");
 $settings_table->addHeader(api_text("cMobileApplicationSetting-property-name"),"nowrap");
 $settings_table->addHeader(api_text("cMobileApplicationSetting-property-value"),null,"100%");

 // cycle all settings
 foreach($application_obj->getSettings() as $setting_fobj){
  // build operation button
  $ob=new strOperationsButton();
  $ob->addElement(api_url(["scr"=>"applications_view","tab"=>"settings","act"=>"setting_edit","idApplication"=>$application_obj->id,"idSetting"=>$setting_fobj->id]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("mobile-manage")));
  if($setting_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cMobileApplicationSetting","idApplication"=>$application_obj->id,"idSetting"=>$setting_fobj->id,"return"=>["scr"=>"applications_view","tab"=>"settings","idApplication"=>$application_obj->id]]),"fa-trash-o",api_text("table-td-undelete"),(api_checkAuthorization("mobile-manage")),api_text("applications_view-settings-td-undelete-confirm"));}
  else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cMobileApplicationSetting","idApplication"=>$application_obj->id,"idSetting"=>$setting_fobj->id,"return"=>["scr"=>"applications_view","tab"=>"settings","idApplication"=>$application_obj->id]]),"fa-trash",api_text("table-td-delete"),(api_checkAuthorization("mobile-manage")),api_text("applications_view-settings-td-delete-confirm"));}
  // make table row class
  $tr_class_array=array();
  if($setting_fobj->id==$_REQUEST['idSetting']){$tr_class_array[]="info";}
  if($setting_fobj->deleted){$tr_class_array[]="deleted";}
  // make mobile row
  $settings_table->addRow(implode(" ",$tr_class_array));
  //$settings_table->addRowFieldAction(api_url(["scr"=>"applications_view","idApplication"=>$application_obj->id]),"fa-search",api_text("table-td-view"));
  $settings_table->addRowField(api_tag("samp",$setting_fobj->code),"nowrap");
  $settings_table->addRowField($setting_fobj->name,"nowrap");
  $settings_table->addRowField(api_tag("samp",$setting_fobj->value),"truncate-ellipsis");
  $settings_table->addRowField($ob->render(),"nowrap text-right");
 }

 // check for version actions
 if(in_array(ACTION,["setting_add","setting_edit"]) && api_checkAuthorization("mobile-manage")){
  // get selected setting
  $selected_setting_obj=new cMobileApplicationSetting($_REQUEST['idSetting']);
  // get form
  $form=$selected_setting_obj->form_edit(["return"=>["scr"=>"applications_view","tab"=>"settings","idApplication"=>$application_obj->id]]);
  // replace fkApplication
  $form->removeField("fkApplication");
  $form->addField("hidden","fkApplication",null,$application_obj->id);
  // additional controls
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build modal
  $modal=new strModal(api_text("applications_view-settings-modal-title-".($selected_setting_obj->id?"edit":"add")),null,"applications_view-settings");
  $modal->setBody($form->render(1));
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_applications_view-settings').modal({show:true,backdrop:'static',keyboard:false});});");
 }

?>