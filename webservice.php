<?php
/**
 * Coordinator MQTT - Web Service
 *
 * @package Coordinator\Modules\MQTT
 * @author  Manuel Zavatta <manuel.zavatta@gmail.com>
 * @link    http://www.coordinator.it
 */
// check for actions
if(!defined('ACTION')){die("ERROR EXECUTING WEB SERVICE: The action was not defined");}

// build return class
$return=new stdClass();
$return->error=false;
$return->errors=array();
$return->result=array();

// switch action
switch(ACTION){
 // applications
 case "application_info":application_info($return);break;
 // operators
 case "operator_info":operator_info($return);break;
 // default
 default:
  // action not found
  $return->error=true;
  //$return->errors[]=array("action_not_found"=>"The action \"".ACTION."\" was not found in \"".MODULE."\" web service");
  $return->errors[]="action_not_found";
}

// encode or debug and return
if(!DEBUG){header("Content-Type: application/json; charset=utf-8");}else{api_dump($return,"RETURN");}
echo json_encode($return,JSON_PRETTY_PRINT);

/**
 * Application Info
 */
function application_info(&$return){
 // get application
 $application_obj=new cMobileApplication();
 $application_obj->loadFromCode($_REQUEST['code']);
 api_dump($application_obj,"application object");
 // check operator
 if(!$application_obj->id){
  // operator not found
  $return->error=true;
  $return->errors[]="application_not_found";
  // return
  return $return;
 }
 // build operator return object
 $application_robj=new stdClass();
 $application_robj->id=(integer)$application_obj->id;
 $application_robj->deleted=(boolean)$application_obj->deleted;
 $application_robj->code=(string)$application_obj->code;
 $application_robj->name=(string)$application_obj->name;
 $application_robj->description=(string)$application_obj->description;
 $application_robj->version=(string)$application_obj->version;
 $application_robj->file=(string)$application_obj->getUrl();
 // build authorizations array
 $authorizations_rarray=array();
 foreach($application_obj->getAuthorizations() as $authorization_fobj){
  if($authorization_fobj->deleted){continue;}
  $authorizations_rarray[]=$authorization_fobj->code;
 }
 // build settings array
 $settings_rarray=array();
 foreach($application_obj->getSettings() as $setting_fobj){
  if($setting_fobj->deleted){continue;}
  $settings_rarray[$setting_fobj->code]=(is_numeric($setting_fobj->value)?(float)$setting_fobj->value:$setting_fobj->value);
 }
 // add application to result
 $return->result["application"]=$application_robj;
 // add authorizations to result
 $return->result["authorizations"]=$authorizations_rarray;
 // add settings to result
 $return->result["settings"]=$settings_rarray;
 // return
 return $return;
}

/**
 * Operator Info
 */
function operator_info(&$return){
 // get operator
 $operator_obj=new cMobileOperator();
 $operator_obj->load_fromBadge($_REQUEST['badge']);
 api_dump($operator_obj,"operator object");
 // check operator
 if(!$operator_obj->id){
  // operator not found
  $return->error=true;
  $return->errors[]="operator_not_found";
  // mail alert
  api_mail_save("Badge ".$_REQUEST['badge']." non abilitato",
   "L'operatore con il badge ".$_REQUEST['badge']." ha tentato di accedere ad un dispositivo mobile senza autorizzazione..",
   "manuel.zavatta@cogne.com;michael.angelini@cogne.com");
  // return
  return $return;
 }
 // build operator return object
 $operator_robj=new stdClass();
 $operator_robj->id=(integer)$operator_obj->id;
 $operator_robj->deleted=(boolean)$operator_obj->deleted;
 $operator_robj->badge=(string)$operator_obj->badge;
 $operator_robj->name=(string)$operator_obj->name;
 $operator_robj->company=(string)$operator_obj->company;
 $operator_robj->function=(string)$operator_obj->function;
 // build authorizations array
 $authorizations_rarray=array();
 // check if user was deleted
 if(!$operator_obj->deleted){
  foreach($operator_obj->getAuthorizations() as $authorization_fobj){
   if($authorization_fobj->deleted){continue;}
   $authorizations_rarray[]=$authorization_fobj->code;
  }
 }
 // add operator to result
 $return->result["operator"]=$operator_robj;
 // add authorizations to result
 $return->result["authorizations"]=$authorizations_rarray;
 // return
 return $return;
}

?>