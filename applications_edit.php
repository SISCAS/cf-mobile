<?php
/**
 * Mobile - Applications Edit
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("mobile-manage","dashboard");
 // get objects
 $application_obj=new cMobileApplication($_REQUEST['idApplication']);
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(($application_obj->id?api_text("applications_edit",$application_obj->name):api_text("applications_edit-add")));
 // get form
 $form=$application_obj->form_edit(["return"=>api_return(["scr"=>"applications_view"])]);
 // additional controls
 if($application_obj->id){
  $form->addControl("button",api_text("form-fc-cancel"),api_return_url(["scr"=>"applications_view","idApplication"=>$application_obj->id]));
  if(!$application_obj->deleted){
   $form->addControl("button",api_text("form-fc-delete"),api_url(["scr"=>"controller","act"=>"delete","obj"=>"cMobileApplication","idApplication"=>$application_obj->id]),"btn-danger",api_text("applications_edit-fc-delete-confirm"));
  }else{
   $form->addControl("button",api_text("form-fc-undelete"),api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cMobileApplication","idApplication"=>$application_obj->id,"return"=>["scr"=>"applications_view"]]),"btn-warning");
   $form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cMobileApplication","idApplication"=>$application_obj->id]),"btn-danger",api_text("applications_edit-fc-remove-confirm"));
  }
 }else{$form->addControl("button",api_text("form-fc-cancel"),api_url(["scr"=>"applications_list"]));}
 // select script
 $app->addScript("$(document).ready(function(){\$('select[name=\"fkEmployee\"]').select2({allowClear:true,placeholder:\"".api_text("cMobileApplication-placeholder-fkEmployee")."\"});});");
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($form->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($application_obj,"application");
?>