<?php
/**
 * Mobile - Controller
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 // debug
 api_dump($_REQUEST,"_REQUEST");
 // check if object controller function exists
 if(function_exists($_REQUEST['obj']."_controller")){
  // call object controller function
  call_user_func($_REQUEST['obj']."_controller",$_REQUEST['act']);
 }else{
  api_alerts_add(api_text("alert_controllerObjectNotFound",[MODULE,$_REQUEST['obj']."_controller"]),"danger");
  api_redirect("?mod=".MODULE);
 }

 /**
  * Application controller
  *
  * @param string $action Object action
  */
 function cMobileApplication_controller($action){
  // check authorizations
  api_checkAuthorization("mobile-manage","dashboard");
  // get object
  $application_obj=new cMobileApplication($_REQUEST['idApplication']);
  api_dump($application_obj,"application object");
  // check object
  if($action!="store" && !$application_obj->id){api_alerts_add(api_text("cMobileApplication-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=applications_list");}
  // execution
  try{
   switch($action){
    case "store":
     $application_obj->store($_REQUEST);
     api_alerts_add(api_text("cMobileApplication-alert-stored"),"success");
     break;
    case "version":
     $application_obj->version($_REQUEST["version"],$_FILES["file"]);
     api_alerts_add(api_text("cMobileApplication-alert-stored"),"success"); /** @todo specific alert? */
     break;
    case "delete":
     $application_obj->delete();
     api_alerts_add(api_text("cMobileApplication-alert-deleted"),"warning");
     break;
    case "undelete":
     $application_obj->undelete();
     api_alerts_add(api_text("cMobileApplication-alert-undeleted"),"warning");
     break;
    case "remove":
     $application_obj->remove();
     api_alerts_add(api_text("cMobileApplication-alert-removed"),"warning");
     break;
    default:
     throw new Exception("Application action \"".$action."\" was not defined..");
   }
   // redirect
   api_redirect(api_return_url(["scr"=>"applications_list","idApplication"=>$application_obj->id]));
  }catch(Exception $e){
   // dump, alert and redirect
   api_redirect_exception($e,api_url(["scr"=>"applications_list","idApplication"=>$application_obj->id]),"cMobileApplication-alert-error");
  }
 }

 /**
  * Application Authorization controller
  *
  * @param string $action Object action
  */
 function cMobileApplicationAuthorization_controller($action){
  // check authorizations
  api_checkAuthorization("mobile-manage","dashboard");
  // get object
  $authorization_obj=new cMobileApplicationAuthorization($_REQUEST['idAuthorization']);
  api_dump($authorization_obj,"application authorization object");
  // check object
  if($action!="store" && !$authorization_obj->id){api_alerts_add(api_text("cMobileApplicationAuthorization-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=applications_list");}
  // execution
  try{
   switch($action){
    case "store":
     $authorization_obj->store($_REQUEST);
     api_alerts_add(api_text("cMobileApplicationAuthorization-alert-stored"),"success");
     break;
    case "delete":
     $authorization_obj->delete();
     api_alerts_add(api_text("cMobileApplicationAuthorization-alert-deleted"),"warning");
     break;
    case "undelete":
     $authorization_obj->undelete();
     api_alerts_add(api_text("cMobileApplicationAuthorization-alert-undeleted"),"warning");
     break;
    case "remove":
     $authorization_obj->remove();
     api_alerts_add(api_text("cMobileApplicationAuthorization-alert-removed"),"warning");
     break;
    default:
     throw new Exception("Application authorization action \"".$action."\" was not defined..");   /** @todo rivedere url di return */
   }
   // redirect
   api_redirect(api_return_url(["scr"=>"applications_view","tab"=>"authorizations","idApplication"=>$authorization_obj->fkApplication,"idAuthorization"=>$authorization_obj->id]));
  }catch(Exception $e){
   // dump, alert and redirect
   api_redirect_exception($e,api_url(["scr"=>"applications_view","tab"=>"authorizations","idApplication"=>$authorization_obj->fkApplication,"idAuthorization"=>$authorization_obj->id]),"cMobileApplicationAuthorization-alert-error");
  }
 }

 /**
  * Application Setting controller
  *
  * @param string $action Object action
  */
 function cMobileApplicationSetting_controller($action){
  // check settings
  api_checkAuthorization("mobile-manage","dashboard");
  // get object
  $setting_obj=new cMobileApplicationSetting($_REQUEST['idSetting']);
  api_dump($setting_obj,"application setting object");
  // check object
  if($action!="store" && !$setting_obj->id){api_alerts_add(api_text("cMobileApplicationSetting-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=applications_list");}
  // execution
  try{
   switch($action){
    case "store":
     $setting_obj->store($_REQUEST);
     api_alerts_add(api_text("cMobileApplicationSetting-alert-stored"),"success");
     break;
    case "delete":
     $setting_obj->delete();
     api_alerts_add(api_text("cMobileApplicationSetting-alert-deleted"),"warning");
     break;
    case "undelete":
     $setting_obj->undelete();
     api_alerts_add(api_text("cMobileApplicationSetting-alert-undeleted"),"warning");
     break;
    case "remove":
     $setting_obj->remove();
     api_alerts_add(api_text("cMobileApplicationSetting-alert-removed"),"warning");
     break;
    default:
     throw new Exception("Application setting action \"".$action."\" was not defined..");   /** @todo rivedere url di return */
   }
   // redirect
   api_redirect(api_return_url(["scr"=>"applications_view","tab"=>"settings","idApplication"=>$setting_obj->fkApplication,"idSetting"=>$setting_obj->id]));
  }catch(Exception $e){
   // dump, alert and redirect
   api_redirect_exception($e,api_url(["scr"=>"applications_view","tab"=>"settings","idApplication"=>$setting_obj->fkApplication,"idSetting"=>$setting_obj->id]),"cMobileApplicationSetting-alert-error");
  }
 }

 /**
  * Operator controller
  *
  * @param string $action Object action
  */
 function cMobileOperator_controller($action){
  // check authorizations
  api_checkAuthorization("mobile-manage","dashboard");
  // get object
  $operator_obj=new cMobileOperator($_REQUEST['idOperator']);
  api_dump($operator_obj,"operator object");
  // check object
  if($action!="store" && !$operator_obj->id){api_alerts_add(api_text("cMobileOperator-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=operators_list");}
  // execution
  try{
   switch($action){
    case "store":
     $operator_obj->store($_REQUEST);
     api_alerts_add(api_text("cMobileOperator-alert-stored"),"success");
     break;
    case "authorizations":
     $operator_obj->store_authorizations($_REQUEST["authorizations"]);
     api_alerts_add(api_text("cMobileOperator-alert-authorizations"),"success");
     break;
    case "delete":
     $operator_obj->delete();
     api_alerts_add(api_text("cMobileOperator-alert-deleted"),"warning");
     break;
    case "undelete":
     $operator_obj->undelete();
     api_alerts_add(api_text("cMobileOperator-alert-undeleted"),"warning");
     break;
    case "remove":
     $operator_obj->remove();
     api_alerts_add(api_text("cMobileOperator-alert-removed"),"warning");
     break;
    default:
     throw new Exception("Operator action \"".$action."\" was not defined..");
   }
   // redirect
   api_redirect(api_return_url(["scr"=>"operators_list","idOperator"=>$operator_obj->id]));
  }catch(Exception $e){
   // dump, alert and redirect
   api_redirect_exception($e,api_url(["scr"=>"operators_list","idOperator"=>$operator_obj->id]),"cMobileOperator-alert-error");
  }
 }

?>