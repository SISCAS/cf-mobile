<?php
/**
 * Mobile - Template
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // build application
 $app=new strApplication();
 // build nav object
 $nav=new strNav("nav-tabs");
 // dashboard
 $nav->addItem(api_icon("fa-th-large",null,"hidden-link"),api_url(["scr"=>"dashboard"]));
 // applications
 if(explode("_",SCRIPT)[0]=="applications"){
  $nav->addItem(api_text("nav-applications-list"),api_url(["scr"=>"applications_list"]));
  // operations
  if($application_obj->id && in_array(SCRIPT,array("applications_view","applications_edit"))){
   $nav->addItem(api_text("nav-operations"),null,null,"active");
   $nav->addSubItem(api_text("nav-applications-operations-version"),api_url(["scr"=>"applications_view","act"=>"version","idApplication"=>$application_obj->id]),(api_checkAuthorization("mobile-manage")));
   $nav->addSubItem(api_text("nav-applications-operations-edit"),api_url(["scr"=>"applications_edit","idApplication"=>$application_obj->id]),(api_checkAuthorization("mobile-manage")));
   $nav->addSubSeparator();
   $nav->addSubItem(api_text("nav-applications-operations-authorization_add"),api_url(["scr"=>"applications_view","tab"=>"authorizations","act"=>"authorization_add","idApplication"=>$application_obj->id]),(api_checkAuthorization("mobile-manage")));
   $nav->addSubItem(api_text("nav-applications-operations-setting_add"),api_url(["scr"=>"applications_view","tab"=>"settings","act"=>"setting_add","idApplication"=>$application_obj->id]),(api_checkAuthorization("mobile-manage")));
  }else{
   $nav->addItem(api_text("nav-applications-add"),api_url(["scr"=>"applications_edit"]),(api_checkAuthorization("mobile-manage")));
  }
 }
 // operators
 if(explode("_",SCRIPT)[0]=="operators"){
  $nav->addItem(api_text("nav-operators-list"),api_url(["scr"=>"operators_list"]));
  // operations
  if($operator_obj->id && in_array(SCRIPT,array("operators_view","operators_edit"))){
   $nav->addItem(api_text("nav-operations"),null,null,"active");
   $nav->addSubItem(api_text("nav-operators-operations-edit"),api_url(["scr"=>"operators_edit","idOperator"=>$operator_obj->id]),(api_checkAuthorization("mobile-manage")));
   $nav->addSubSeparator();
   $nav->addSubItem(api_text("nav-operators-operations-authorizations"),api_url(["scr"=>"operators_view","tab"=>"authorizations","act"=>"authorizations","idOperator"=>$operator_obj->id]),(api_checkAuthorization("mobile-manage")));
  }else{
   $nav->addItem(api_text("nav-operators-add"),api_url(["scr"=>"operators_edit"]),(api_checkAuthorization("mobile-manage")));
  }
 }
 // add nav to html
 $app->addContent($nav->render(false));
?>