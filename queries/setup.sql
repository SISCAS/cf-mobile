--
-- Mobile - Setup (1.0.0)
--
-- @package Coordinator\Modules\Mobile
-- @company Cogne Acciai Speciali s.p.a
-- @authors Manuel Zavatta <manuel.zavatta@cogne.com>
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `mobile__applications`
--

CREATE TABLE IF NOT EXISTS `mobile__applications` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `mobile__applications__logs`
--

CREATE TABLE IF NOT EXISTS `mobile__applications__logs` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fkObject` int(11) UNSIGNED NOT NULL,
  `fkUser` int(11) UNSIGNED DEFAULT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL,
  `alert` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `properties_json` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `fkObject` (`fkObject`),
  CONSTRAINT `mobile__applications__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `mobile__applications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `mobile__applications__authorizations`
--

CREATE TABLE IF NOT EXISTS `mobile__applications__authorizations` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `fkApplication` int(11) UNSIGNED NOT NULL,
  `code` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `fkApplication` (`fkApplication`),
  CONSTRAINT `mobile__applications__authorizations_ibfk_1` FOREIGN KEY (`fkApplication`) REFERENCES `mobile__applications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `mobile__applications__settings`
--

CREATE TABLE IF NOT EXISTS `mobile__applications__settings` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `fkApplication` int(11) UNSIGNED NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`fkApplication`,`code`) USING BTREE,
  KEY `fkApplication` (`fkApplication`),
  CONSTRAINT `mobile__applications__settings_ibfk_1` FOREIGN KEY (`fkApplication`) REFERENCES `mobile__applications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mobile__operators`
--

CREATE TABLE IF NOT EXISTS `mobile__operators` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `badge` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `function` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `badge` (`badge`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `mobile__operators__logs`
--

CREATE TABLE IF NOT EXISTS `mobile__operators__logs` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fkObject` int(11) UNSIGNED NOT NULL,
  `fkUser` int(11) UNSIGNED DEFAULT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL,
  `alert` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `properties_json` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `fkObject` (`fkObject`),
  CONSTRAINT `mobile__operators__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `mobile__operators` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `mobile__operators__join__authorizations`
--

CREATE TABLE IF NOT EXISTS `mobile__operators__join__authorizations` (
  `fkOperator` int(11) UNSIGNED NOT NULL,
  `fkAuthorization` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`fkOperator`,`fkAuthorization`),
  KEY `fkOperator` (`fkOperator`),
  KEY `fkAuthorization` (`fkAuthorization`),
  CONSTRAINT `mobile__operators__join__authorizations_ibfk_2` FOREIGN KEY (`fkAuthorization`) REFERENCES `mobile__applications__authorizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mobile__operators__join__authorizations_ibfk_1` FOREIGN KEY (`fkOperator`) REFERENCES `mobile__operators` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Authorizations
--

INSERT IGNORE INTO `framework__modules__authorizations` (`id`,`fkModule`,`order`) VALUES
('mobile-manage','mobile',1),
('mobile-usage','mobile',2);

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
