<?php
/**
 * Mobile - Application
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 /**
  * Mobile Application class
  */
 class cMobileApplication extends cObject{

  /** Parameters */
  static protected $table="mobile__applications";
  static protected $logs=true;

  /** Properties */
  protected $id;
  protected $deleted;
  protected $code;
  protected $name;
  protected $description;
  protected $version;
  protected $file;

  /**
   * Decode log properties
   *
   * {@inheritdoc}
   */
  public static function log_decode($event,$properties){
   // make return array
   $return_array=array();
   // authorization events
   if($properties['idAuthorization']){
    $return_array[]=api_text("cMobileApplicationAuthorization").": ".(new cMobileApplicationAuthorization($properties['idAuthorization']))->code;
    if($properties['code']){$return_array[]=api_text("cMobileApplicationAuthorization-property-code").": ".$properties['code']['previous']." &rarr; ".$properties['code']['current'];}
   }
   // setting events
   elseif($properties['idSetting']){
    $return_array[]=api_text("cMobileApplicationSetting").": ".(new cMobileApplicationSetting($properties['idSetting']))->code;
    if($properties['code']){$return_array[]=api_text("cMobileApplicationSetting-property-code").": ".$properties['code']['previous']." &rarr; ".$properties['code']['current'];}
   }
   // application events
   else{
     if($properties['version']){$return_array[]=api_text("cMobileApplication-property-version").": ".$properties['version']['previous']." &rarr; ".$properties['version']['current'];}
   }
   // return
   return implode(" | ",$return_array);
  }

  /**
   * Load from Code
   *
   * @param string $code Authorization code
   * @return boolean
   */
  public function loadFromCode($code){return parent::loadFromKey("code",$code);}

  /**
   * Get URL
   *
   * @return string|false
   */
  public function getUrl(){
   if(!file_exists(DIR."uploads/mobile/applications/".$this->file)){return false;}
   return URL."uploads/mobile/applications/".$this->file;
  }

  /**
   * Get Authorizations
   */
  public function getAuthorizations(){return api_sortObjectsArray(cMobileApplicationAuthorization::availables(true,["fkApplication"=>$this->id]),"code");}

  /**
   * Get Settings
   */
  public function getSettings(){return api_sortObjectsArray(cMobileApplicationSetting::availables(true,["fkApplication"=>$this->id]),"code");}

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
   // check properties
   if(!strlen(trim($this->code))){throw new Exception("Application code is mandatory..");}
   if(!strlen(trim($this->name))){throw new Exception("Application name is mandatory..");}
   // return
   return true;
  }

  /**
   * Store
   *
   * @param mixed[] $properties Array of properties
   * @param boolean $log Log event
   * @return boolean
   */
  public function store(array $properties,$log=true){
   // clean code property
   if($properties['code']){$properties['code']=strtolower(str_replace(" ","-",trim(api_cleanString($_REQUEST['code']))));}
   // call parent
   return parent::store($properties,$log);
  }

  /**
   * Version
   *
   * @param mixed[] $properties Array of properties
   * @param boolean $log Log event
   * @return boolean
   */
  public function version($version,$upload,$log=true){
   // checks parameters
   //if(!is_array($upload) || !is_uploaded_file($upload['tmp_name']) || $upload['error'] || !$upload['size']){throw new Exception("Upload file is mandatory..");}
   if(!api_uploads_check($upload)){throw new Exception("Upload file is mandatory..");}
   if(!$version){throw new Exception("Application version is mandatory..");}
   // make file
   $file=$this->code."_".$version.".".end((explode(".",$upload["name"])));
   // store uploaded file
   if(!api_uploads_store($upload,"mobile/applications",$file,true)){throw new Exception("Error uploading file..");}
   // delete previous version
   if($version!=$this->version){api_uploads_remove("mobile/applications",$this->file);}
   // call parent
   return parent::store(["version"=>$version,"file"=>$file],$log);
  }

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=null){
   // build form
   $form=new strForm(api_url(array_merge(["mod"=>"mobile","scr"=>"controller","act"=>"store","obj"=>"cMobileApplication","idApplication"=>$this->id],$additional_parameters)),"POST",null,null,"mobile_application_edit_form");
   // fields
   $form->addField("text","code",api_text("cMobileApplication-property-code"),$this->code,api_text("cMobileApplication-placeholder-code"),null,null,null,"required".($this->exists()?" readonly":null));
   $form->addField("text","name",api_text("cMobileApplication-property-name"),$this->name,api_text("cMobileApplication-placeholder-name"),null,null,null,"required");
   $form->addField("textarea","description",api_text("cMobileApplication-property-description"),$this->description,api_text("cMobileApplication-placeholder-description"),null,null,null,"rows='2'");
   // controls
   $form->addControl("submit",api_text("form-fc-submit"));
   // return
   return $form;
  }

  /**
   * Version form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_version(array $additional_parameters=null){
   // build form
   $form=new strForm(api_url(array_merge(["mod"=>"mobile","scr"=>"controller","act"=>"version","obj"=>"cMobileApplication","idApplication"=>$this->id],$additional_parameters)),"POST",null,null,"mobile_application_version_form");
   // inputs
   $form->addField("text","version",api_text("cMobileApplication-property-version"),$this->version,null,null,null,null,"required");
   $form->addField("file","file",api_text("cMobileApplication-property-file"),null,null,null,null,null,"required");
   // controls
   $form->addControl("submit",api_text("form-fc-submit"));
   // return
   return $form;
  }

  // Disable remove function
  public function remove(){throw new Exception("Application remove function disabled by developer..");}

  // debug
  //protected function event_triggered($event){api_dump($event,static::class." event triggered");}

 }

?>