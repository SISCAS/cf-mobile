<?php
/**
 * Mobile
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // definitions
 $module_name="mobile";
 $module_repository_url="https://bitbucket.org/SISCAS/cf-mobile/";
 $module_repository_version_url="https://bitbucket.org/SISCAS/cf-mobile/raw/master/VERSION.txt";
 $module_required_modules=array();
?>