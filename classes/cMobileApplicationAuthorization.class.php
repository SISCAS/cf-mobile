<?php
/**
 * Mobile - Application Authorization
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 /**
  * Mobile Application Authorization class
  */
 class cMobileApplicationAuthorization extends cObject{

  /** Parameters */
  static protected $table="mobile__applications__authorizations";
  static protected $logs=false;

  /** Properties */
  protected $id;
  protected $deleted;
  protected $fkApplication;
  protected $code;
  protected $name;

  /**
   * Load from Code
   *
   * @param string $code Authorization code
   * @return boolean
   */
  public function loadFromCode($code){return parent::loadFromKey("code",$code);}

  /**
   * Get Application
   *
   * @return object
   */
  public function getApplication(){return new cMobileApplication($this->fkApplication);}

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
   // check properties
   if(!strlen(trim($this->fkApplication))){throw new Exception("Authorization application key is mandatory..");}
   if(!strlen(trim($this->code))){throw new Exception("Authorization code is mandatory..");}
   if(!strlen(trim($this->name))){throw new Exception("Authorization name is mandatory..");}
   // return
   return true;
  }

  /**
   * Store
   *
   * @param mixed[] $properties Array of properties
   * @param boolean $log Log event
   * @return boolean
   */
  public function store(array $properties,$log=true){
   // clean code property
   if($properties['code']){$properties['code']=strtolower(str_replace(" ","-",trim(api_cleanString($_REQUEST['code']))));}
   // call parent
   return parent::store($properties,$log);
  }

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=null){
   // build form
   $form=new strForm(api_url(array_merge(["mod"=>"mobile","scr"=>"controller","act"=>"store","obj"=>"cMobileApplicationAuthorization","idAuthorization"=>$this->id],$additional_parameters)),"POST",null,null,"mobile_authorization_edit_form");
   // fields
   $form->addField("select","fkApplication",api_text("cMobileApplicationAuthorization-property-fkApplication"),$this->fkApplication,api_text("cMobileApplicationAuthorization-placeholder-fkApplication"),null,null,null,"required");
   foreach(cMobileApplication::availables(true) as $application_fobj){$form->addFieldOption($application_fobj->id,$application_fobj->name);}
   $form->addField("text","code",api_text("cMobileApplicationAuthorization-property-code"),$this->code,api_text("cMobileApplicationAuthorization-placeholder-code"),null,null,null,"required");
   $form->addField("text","name",api_text("cMobileApplicationAuthorization-property-name"),$this->name,api_text("cMobileApplicationAuthorization-placeholder-name"),null,null,null,"required");
   // controls
   $form->addControl("submit",api_text("form-fc-submit"));
   // return
   return $form;
  }

  // Disable remove function
  public function remove(){throw new Exception("Authorization remove function disabled by developer..");}

  // debug
  protected function event_triggered($event){
   //api_dump($event,static::class." event triggered");
   // skip trace events
   if($event->typology=="trace"){return;}
   // log event to application
   $this->getApplication()->event_log($event->typology,$event->action,array_merge(["idAuthorization"=>$this->id],$event->properties));
  }

 }

?>