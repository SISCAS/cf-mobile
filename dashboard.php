<?php
/**
 * Mobile - Dashboard
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("mobile"));
 // build dashboard object
 $dashboard=new strDashboard();
 $dashboard->addTile(api_url(["scr"=>"applications_list"]),api_text("dashboard-applications-list"),api_text("dashboard-applications-list-description"),(api_checkAuthorization("mobile-usage")),"1x1","fa-th-large");
 $dashboard->addTile(api_url(["scr"=>"operators_list"]),api_text("dashboard-operators-list"),api_text("dashboard-operators-list-description"),(api_checkAuthorization("mobile-usage")),"1x1","fa-group");
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($dashboard->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
?>