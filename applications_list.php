<?php
/**
 * Mobile - Applications List
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("mobile-usage","dashboard");
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // definitions
 $users_array=array();
 // set application title
 $app->setTitle(api_text("applications_list"));
 // definitions
 $applications_array=array();
 // build filter
 $filter=new strFilter();
 $filter->addSearch(["code","name","description"]);
 // build query object
 $query=new cQuery("mobile__applications",$filter->getQueryWhere());
 $query->addQueryOrderField("code");
 // build pagination object
 $pagination=new strPagination($query->getRecordsCount());
 // cycle all results
 foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$applications_array[$result_f->id]=new cMobileApplication($result_f);}
 // build table
 $table=new strTable(api_text("applications_list-tr-unvalued"));
 $table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
 $table->addHeader(api_text("applications_list-th-code"),"nowrap");
 $table->addHeader(api_text("applications_list-th-name"),null,"100%");
 $table->addHeader("&nbsp;",null,16);
 // cycle all applications
 foreach($applications_array as $application_fobj){
  // build operation button
  $ob=new strOperationsButton();
  $ob->addElement(api_url(["scr"=>"applications_edit","idApplication"=>$application_fobj->id,"return"=>["scr"=>"applications_list"]]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("mobile-manage")));
  if($application_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cMobileApplication","idApplication"=>$application_fobj->id,"return"=>["scr"=>"applications_list"]]),"fa-trash-o",api_text("table-td-undelete"),(api_checkAuthorization("mobile-manage")),api_text("applications_list-td-undelete-confirm"));}
  else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cMobileApplication","idApplication"=>$application_fobj->id,"return"=>["scr"=>"applications_list"]]),"fa-trash",api_text("table-td-delete"),(api_checkAuthorization("mobile-manage")),api_text("applications_list-td-delete-confirm"));}
  // make table row class
  $tr_class_array=array();
  if($application_fobj->id==$_REQUEST['idApplication']){$tr_class_array[]="info";}
  if($application_fobj->deleted){$tr_class_array[]="deleted";}
  // make mobile row
  $table->addRow(implode(" ",$tr_class_array));
  $table->addRowFieldAction(api_url(["scr"=>"applications_view","idApplication"=>$application_fobj->id]),"fa-search",api_text("table-td-view"));
  $table->addRowField(api_tag("samp",$application_fobj->code),"nowrap");
  $table->addRowField($application_fobj->name,"truncate-ellipsis");
  $table->addRowField($ob->render(),"nowrap text-right");
 }
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($filter->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($table->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($pagination->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($query,"query");
?>