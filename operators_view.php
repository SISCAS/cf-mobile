<?php
/**
 * Mobile - Operators View
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("mobile-usage","dashboard");
 // get objects
 $operator_obj=new cMobileOperator($_REQUEST['idOperator']);
 // check objects
 if(!$operator_obj->id){api_alerts_add(api_text("cMobileOperator-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=operators_list");}
 // deleted alert
 if($operator_obj->deleted){api_alerts_add(api_text("cMobileOperator-warning-deleted"),"warning");}
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("operators_view",$operator_obj->name));
 // check for tab
 if(!defined(TAB)){define("TAB","informations");}
 // build operators description list
 $dl=new strDescriptionList("br","dl-horizontal");
 $dl->addElement(api_text("cMobileOperator-property-badge"),api_tag("samp",$operator_obj->getBadge()));
 $dl->addElement(api_text("cMobileOperator-property-name"),api_tag("strong",$operator_obj->name));
 // include tabs
 require_once(MODULE_PATH."operators_view-informations.inc.php");
 require_once(MODULE_PATH."operators_view-authorizations.inc.php");
 // build view tabs
 $tab=new strTab();
 $tab->addItem(api_icon("fa-flag-o")." ".api_text("operators_view-tab-informations"),$informations_dl->render(),("informations"==TAB?"active":null));
 $tab->addItem(api_icon("fa-check-square-o")." ".api_text("operators_view-tab-authorizations"),$authorizations_table->render(),("authorizations"==TAB?"active":null));
 $tab->addItem(api_icon("fa-file-text-o")." ".api_text("operators_view-tab-logs"),api_logs_table($operator_obj->getLogs((!$_REQUEST['all_logs']?10:null)))->render(),("logs"==TAB?"active":null));
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($dl->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($tab->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($operator_obj,"operator");
?>