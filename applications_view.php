<?php
/**
 * Mobile - Applications View
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("mobile-usage","dashboard");
 // get objects
 $application_obj=new cMobileApplication($_REQUEST['idApplication']);
 // check objects
 if(!$application_obj->id){api_alerts_add(api_text("cMobileApplication-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=applications_list");}
 // deleted alert
 if($application_obj->deleted){api_alerts_add(api_text("cMobileApplication-warning-deleted"),"warning");}
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("applications_view",$application_obj->name));
 // check for tab
 if(!defined(TAB)){define("TAB","informations");}
 // build applications description list
 $left_dl=new strDescriptionList("br","dl-horizontal");
 $left_dl->addElement(api_text("cMobileApplication-property-code"),api_tag("samp",$application_obj->code));
 $left_dl->addElement(api_text("cMobileApplication-property-name"),api_tag("strong",$application_obj->name));
 // build right description list
 $right_dl=new strDescriptionList("br","dl-horizontal");
 if($application_obj->description){$right_dl->addElement(api_text("cMobileApplication-property-description"),nl2br($application_obj->description));}
 // include tabs
 require_once(MODULE_PATH."applications_view-informations.inc.php");
 require_once(MODULE_PATH."applications_view-authorizations.inc.php");
 require_once(MODULE_PATH."applications_view-settings.inc.php");
 // build view tabs
 $tab=new strTab();
 $tab->addItem(api_icon("fa-flag-o")." ".api_text("applications_view-tab-informations"),$informations_dl->render(),("informations"==TAB?"active":null));
 $tab->addItem(api_icon("fa-check-square-o")." ".api_text("applications_view-tab-authorizations"),$authorizations_table->render(),("authorizations"==TAB?"active":null));
 $tab->addItem(api_icon("fa-sliders")." ".api_text("applications_view-tab-settings"),$settings_table->render(),("settings"==TAB?"active":null));
 $tab->addItem(api_icon("fa-file-text-o")." ".api_text("applications_view-tab-logs"),api_logs_table($application_obj->getLogs((!$_REQUEST['all_logs']?10:null)))->render(),("logs"==TAB?"active":null));
 // check for version actions
 if(ACTION=="version" && api_checkAuthorization("mobile-manage")){
  // get form
  $form=$application_obj->form_version(["return"=>["scr"=>"applications_view"]]);
  // additional controls
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build modal
  $modal=new strModal(api_text("applications_view-version-modal-title"),null,"applications_view-version");
  $modal->setBody($form->render(2));
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_applications_view-version').modal({show:true,backdrop:'static',keyboard:false});});");
 }
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($left_dl->render(),"col-xs-12 col-md-5");
 $grid->addCol($right_dl->render(),"col-xs-12 col-md-7");
 $grid->addRow();
 $grid->addCol($tab->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($selected_authorization_obj,"selected authorization");
 api_dump($selected_setting_obj,"selected setting");
 api_dump($application_obj,"application");
?>