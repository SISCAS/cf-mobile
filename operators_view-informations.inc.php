<?php
/**
 * Mobile - Operators View (Informations)
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 // build informations description list
 $informations_dl=new strDescriptionList("br","dl-horizontal");
 $informations_dl->addElement(api_text("cMobileOperator-property-company"),$operator_obj->company);
 $informations_dl->addElement(api_text("cMobileOperator-property-function"),$operator_obj->function);

?>