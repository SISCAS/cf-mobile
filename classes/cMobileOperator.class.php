<?php
/**
 * Mobile - Operator
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 /**
  * Mobile Operator class
  */
 class cMobileOperator extends cObject{

  /** Parameters */
  static protected $table="mobile__operators";
  static protected $logs=true;

  /** Properties */
  protected $id;
  protected $deleted;
  protected $badge;
  protected $name;
  protected $company;
  protected $function;

  /**
   * Debadge log properties
   *
   * {@inheritdoc}
   */
  public static function log_decode($event,$properties){
   switch($event){
    case "updated":
     $return_array=array();
     if($properties['badge']){$return_array[]=api_text("cMobileOperator-property-badge").": ".$properties['badge']['previous']." &rarr; ".$properties['badge']['current'];}
     $return=implode(" | ",$return_array);
     break;
    case "authorizations":
     $return_array=array();
     foreach($properties['current'] as $authorization_f){$return_array[]=(new cMobileApplicationAuthorization($authorization_f))->code;}
     $return=api_tag("small",implode(", ",$return_array));
     break;
   }
   // return
   return $return;
  }

  /**
   * Load from Badge
   *
   * @param string $badge Operator badge
   * @return boolean
   */
  public function load_fromBadge($badge){
   // checks
   if(strlen($badge)!=8){return false;}
   if($this->id){return false;}
   // make query
   $query="SELECT * FROM `".static::$table."` WHERE `badge`='".$badge."'";
   //api_dump($query,static::class."->load_fromBadge query");
   // get object from database
   $object=$GLOBALS['database']->queryUniqueObject($query);
   // call parent load
   return parent::load($object);
  }

  /**
   * Get Badge
   *
   * @return string
   */
  public function getBadge(){return rtrim(chunk_split($this->badge,2,':'),":");}

  /**
   * Get Authorizations
   *
   * @return object[]
   */
  public function getAuthorizations(){return api_sortObjectsArray($this->joined_select("mobile__operators__join__authorizations","fkOperator","cMobileApplicationAuthorization","fkAuthorization"),"code");}

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
   // check properties
   if(!strlen(trim($this->badge))){throw new Exception("Operator badge is mandatory..");}
   if(!strlen(trim($this->name))){throw new Exception("Operator name is mandatory..");}
   // return
   return true;
  }

  /**
   * Store
   *
   * @param mixed[] $properties Array of properties
   * @param boolean $log Log event
   * @return boolean
   */
  public function store(array $properties,$log=true){
   // clean badge property
   if($properties['badge']){$properties['badge']=api_cleanString(strtoupper($_REQUEST['badge']),"/[^A-F0-9]/");}
   // call parent
   return parent::store($properties,$log);
  }

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=null){
   // build form
   $form=new strForm(api_url(array_merge(["mod"=>"mobile","scr"=>"controller","act"=>"store","obj"=>"cMobileOperator","idOperator"=>$this->id],$additional_parameters)),"POST",null,null,"mobile_operator_edit_form");
   // fields
   $form->addField("text","badge",api_text("cMobileOperator-property-badge"),$this->getBadge(),api_text("cMobileOperator-placeholder-badge"),null,null,null,"required");
   $form->addField("text","name",api_text("cMobileOperator-property-name"),$this->name,api_text("cMobileOperator-placeholder-name"),null,null,null,"required");
   $form->addField("text","company",api_text("cMobileOperator-property-company"),$this->company,api_text("cMobileOperator-placeholder-company"));
   $form->addField("text","function",api_text("cMobileOperator-property-function"),$this->function,api_text("cMobileOperator-placeholder-function"));
   // controls
   $form->addControl("submit",api_text("form-fc-submit"));
   // return
   return $form;
  }

  /**
   * Authorizations form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_authorizations(array $additional_parameters=null){
   // build form
   $form=new strForm(api_url(array_merge(["mod"=>"mobile","scr"=>"controller","act"=>"authorizations","obj"=>"cMobileOperator","idOperator"=>$this->id],$additional_parameters)),"POST",null,null,"mobile_operators_authorizations_form");
   // fields
   $form->addField("checkbox","authorizations[]",null,array_keys($this->getAuthorizations()));
   // cycle all authorizations
   foreach(api_sortObjectsArray(cMobileApplicationAuthorization::availables(true),"code") as $authorization_fobj){
    $form->addFieldOption($authorization_fobj->id,api_tag("samp",$authorization_fobj->code)." &rarr; ".$authorization_fobj->name);
   }
   // controls
   $form->addControl("submit",api_text("form-fc-submit"));
   // return
   return $form;
  }

  /**
   * Store Authorizations
   *
   * @param boolean $log Log event
   * @return boolean
   */
  public function store_authorizations($authorizations,$log=true){
   // check existence
   if(!$this->exists()){return false;}
   // check for array
   if(!is_array($authorizations)){return false;}
   // make event properties
   $event_properties_array=array("previous"=>array_keys($this->getAuthorizations()),"current"=>$authorizations);
   // reset authorizations
   $this->joined_reset("mobile__operators__join__authorizations","fkOperator",null,false);
   // cycle all authorizations
   foreach($authorizations as $authorization_f){
    // get object
    $authorization_fobj=new cMobileApplicationAuthorization($authorization_f);
    // check object
    if(!$authorization_fobj->id){continue;}
    // add authorization to operator
    $this->joined_add("mobile__operators__join__authorizations","fkOperator","cMobileApplicationAuthorization","fkAuthorization",$authorization_fobj,null,false);
   }
   // throw event
   $this->event("information","authorizations",$event_properties_array,$log);
   // return
   return true;
  }

  // Disable remove function
  public function remove(){throw new Exception("Operator remove function disabled by developer..");}

  // debug
  //protected function event_triggered($event){api_dump($event,static::class." event triggered");}

 }

?>