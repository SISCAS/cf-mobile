<?php
/**
 * Mobile - Operators View (Authorizations)
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 // build authorizations array
 $authorizations_array=array();
 foreach($operator_obj->getAuthorizations() as $authorization_fobj){
  $authorizations_array[$authorization_fobj->getApplication()->code][]=$authorization_fobj->code;
 }
 
 // build authorizations description list
 $authorizations_table=new strTable(api_text("operators_view-authorizations-tr-unvalued"));
 //$authorizations_table->addHeader("&nbsp;","nowrap",16);
 $authorizations_table->addHeader(api_text("operators_view-authorizations-th-code"),"nowrap");
 $authorizations_table->addHeader(api_text("operators_view-authorizations-th-authorization"),null,"100%");
 // cycle all authorizations
 foreach($operator_obj->getAuthorizations() as $authorization_fobj){
  // make table row class
  $tr_class_array=array();
  if($authorization_fobj->id==$_REQUEST['idAuthorization']){$tr_class_array[]="info";}
  if($authorization_fobj->deleted){$tr_class_array[]="deleted";}
  // make mobile row
  $authorizations_table->addRow(implode(" ",$tr_class_array));
  //$authorizations_table->addRowFieldAction(api_url(["scr"=>"applications_view","tab"=>"authorizations","idApplication"=>$authorization_fobj->fkApplication,"idAuthorization"=>$authorization_fobj->id]),"fa-search",api_text("table-td-view"),null,null,null,null,"_blank");
  $authorizations_table->addRowField(api_tag("samp",$authorization_fobj->code),"nowrap");
  $authorizations_table->addRowField($authorization_fobj->getApplication()->name." &rarr; ".$authorization_fobj->name,"truncate-ellipsis");
 }

 // check for authorizations actions
 if(ACTION=="authorizations" && api_checkAuthorization("mobile-manage")){
  // get form
  $form=$operator_obj->form_authorizations(["return"=>["scr"=>"operators_view","tab"=>"authorizations"]]);
  // additional controls
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build modal
  $modal=new strModal(api_text("operators_view-authorizations-modal-title",$operator_obj->name),null,"operators_view-authorizations");
  $modal->setBody($form->render());
  // add modal to operator
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_operators_view-authorizations').modal({show:true,backdrop:'static',keyboard:false});});");
 }

?>