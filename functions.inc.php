<?php
/**
 * Mobile Functions
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // include classes
 require_once(DIR."modules/mobile/classes/cMobileApplication.class.php");
 require_once(DIR."modules/mobile/classes/cMobileApplicationAuthorization.class.php");
 require_once(DIR."modules/mobile/classes/cMobileApplicationSetting.class.php");
 require_once(DIR."modules/mobile/classes/cMobileOperator.class.php");
?>