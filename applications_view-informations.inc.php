<?php
/**
 * Mobile - Applications View (Informations)
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 // build informations description list
 $informations_dl=new strDescriptionList("br","dl-horizontal");
 $informations_dl->addElement(api_text("cMobileApplication-property-version"),api_tag("samp",$application_obj->version));
 $informations_dl->addElement(api_text("applications_view-dl-download"),api_link($application_obj->getUrl(),api_tag("samp",$application_obj->file),api_text("applications_view-dt-download")));

?>