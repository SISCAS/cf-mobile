<?php
/**
 * Mobile - Operators Edit
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("mobile-manage","dashboard");
 // get objects
 $operator_obj=new cMobileOperator($_REQUEST['idOperator']);
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(($operator_obj->id?api_text("operators_edit",$operator_obj->name):api_text("operators_edit-add")));
 // get form
 $form=$operator_obj->form_edit(["return"=>api_return(["scr"=>"operators_view"])]);
 // additional controls
 if($operator_obj->id){
  $form->addControl("button",api_text("form-fc-cancel"),api_return_url(["scr"=>"operators_view","idOperator"=>$operator_obj->id]));
  if(!$operator_obj->deleted){
   $form->addControl("button",api_text("form-fc-delete"),api_url(["scr"=>"controller","act"=>"delete","obj"=>"cMobileOperator","idOperator"=>$operator_obj->id]),"btn-danger",api_text("operators_edit-fc-delete-confirm"));
  }else{
   $form->addControl("button",api_text("form-fc-undelete"),api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cMobileOperator","idOperator"=>$operator_obj->id,"return"=>["scr"=>"operators_view"]]),"btn-warning");
   $form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cMobileOperator","idOperator"=>$operator_obj->id]),"btn-danger",api_text("operators_edit-fc-remove-confirm"));
  }
 }else{$form->addControl("button",api_text("form-fc-cancel"),api_url(["scr"=>"operators_list"]));}
 // select script
 $app->addScript("$(document).ready(function(){\$('select[name=\"fkEmployee\"]').select2({allowClear:true,placeholder:\"".api_text("cMobileOperator-placeholder-fkEmployee")."\"});});");
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($form->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($operator_obj,"operator");
?>