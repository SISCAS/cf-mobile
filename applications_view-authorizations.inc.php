<?php
/**
 * Mobile - Applications View (Authorizations)
 *
 * @package Coordinator\Modules\Mobile
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 // build authorizations description list
 $authorizations_table=new strTable(api_text("applications_view-authorizations-tr-unvalued"));
 $authorizations_table->addHeader(api_text("cMobileApplicationAuthorization-property-code"),"nowrap");
 $authorizations_table->addHeader(api_text("cMobileApplicationAuthorization-property-name"),null,"100%");

 // cycle all authorizations
 foreach($application_obj->getAuthorizations() as $authorization_fobj){
  // build operation button
  $ob=new strOperationsButton();
  $ob->addElement(api_url(["scr"=>"applications_view","tab"=>"authorizations","act"=>"authorization_edit","idApplication"=>$application_obj->id,"idAuthorization"=>$authorization_fobj->id]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("mobile-manage")));
  if($authorization_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cMobileApplicationAuthorization","idApplication"=>$application_obj->id,"idAuthorization"=>$authorization_fobj->id,"return"=>["scr"=>"applications_view","tab"=>"authorizations","idApplication"=>$application_obj->id]]),"fa-trash-o",api_text("table-td-undelete"),(api_checkAuthorization("mobile-manage")),api_text("applications_view-authorizations-td-undelete-confirm"));}
  else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cMobileApplicationAuthorization","idApplication"=>$application_obj->id,"idAuthorization"=>$authorization_fobj->id,"return"=>["scr"=>"applications_view","tab"=>"authorizations","idApplication"=>$application_obj->id]]),"fa-trash",api_text("table-td-delete"),(api_checkAuthorization("mobile-manage")),api_text("applications_view-authorizations-td-delete-confirm"));}
  // make table row class
  $tr_class_array=array();
  if($authorization_fobj->id==$_REQUEST['idAuthorization']){$tr_class_array[]="info";}
  if($authorization_fobj->deleted){$tr_class_array[]="deleted";}
  // make mobile row
  $authorizations_table->addRow(implode(" ",$tr_class_array));
  $authorizations_table->addRowField(api_tag("samp",$authorization_fobj->code),"nowrap");
  $authorizations_table->addRowField($authorization_fobj->name,"truncate-ellipsis");
  $authorizations_table->addRowField($ob->render(),"nowrap text-right");
 }

 // check for version actions
 if(in_array(ACTION,["authorization_add","authorization_edit"]) && api_checkAuthorization("mobile-manage")){
  // get selected authorization
  $selected_authorization_obj=new cMobileApplicationAuthorization($_REQUEST['idAuthorization']);
  // get form
  $form=$selected_authorization_obj->form_edit(["return"=>["scr"=>"applications_view","tab"=>"authorizations","idApplication"=>$application_obj->id]]);
  // replace fkApplication
  $form->removeField("fkApplication");
  $form->addField("hidden","fkApplication",null,$application_obj->id);
  // additional controls
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build modal
  $modal=new strModal(api_text("applications_view-authorizations-modal-title-".($selected_authorization_obj->id?"edit":"add")),null,"applications_view-authorizations");
  $modal->setBody($form->render(1));
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_applications_view-authorizations').modal({show:true,backdrop:'static',keyboard:false});});");
 }

?>